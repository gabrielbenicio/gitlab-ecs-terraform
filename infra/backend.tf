terraform {
  backend "remote" {
    hostname     = "app.terraform.io"
    organization = "gabrielbeniciolopes"

    workspaces {
      name = "gitlab-ecs-terraform"
    }
  }
}

